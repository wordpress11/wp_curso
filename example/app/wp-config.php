<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'db:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'R6,7^&`+9T/&uhx#}cIyO<j#jpXs*8G,lk>%/wIJN`u_tt%o)Plr^[*Cp`D,n)m0' );
define( 'SECURE_AUTH_KEY',   '`CJ92-jN]9H(Q`n`RD5,.u%i.|m}u#0]hvR2s]ltE}6=Z_Tt{Tkd^C+JM,il<wzQ' );
define( 'LOGGED_IN_KEY',     'vDEL^j1+{Ke)$qPZ+C>L$pT7y9(Y+:67<~2|M,Y(xwULdZ*g-hzA#}fi>?[@,Gks' );
define( 'NONCE_KEY',         'B,)fL;}6n`CfRse[5:Fl1R=Qj8pG~cBOB * +j`I?m,Q|mgBB5nIlr5]ajloN0Os' );
define( 'AUTH_SALT',         'F-Z>ZtU6[tjG P|Qt=s_9*Xch7s>n#K;ehg`-qXQ{5<~P()pIu>@Zp]cmfiHdABa' );
define( 'SECURE_AUTH_SALT',  '[h0Y_fDBJ6`xRe+3B!b[wrX^!kG8de}TQ2xu+3% 7i=[52LMW,>CQsE]S>NbR-Z8' );
define( 'LOGGED_IN_SALT',    '9p=4FKy=TL*gqB,8}*%v[ftJ%7*k6.0u=3>[Ez!#<l9uD<bl4.R!#8LcdIFaJ#RG' );
define( 'NONCE_SALT',        ',BWtlFP;Ya=8Li?)S?Vzz*j?JZu?>:q~2)W0D44}VkLTRJcQO7s+UviKp<s:=lx1' );
define( 'WP_CACHE_KEY_SALT', '8g`jQ; 22:up+9t46+_29!vwLnm},IG3w-s%ge>jz2@$.Ne~tUCd-aKkxE*;WU)F' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
